package com.example.user.senarivessel;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class Search extends Fragment {
    EditText search;

    // ...
    // Define the listener of the interface type
    // listener is the activity itself
    private OnItemSelectedListener listener;

    // Define the events that the fragment will use to communicate
    public interface OnItemSelectedListener {
        public void OnItemSelected(String link);
    }

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }
    }

    public Search() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflatedView=inflater.inflate(R.layout.fragment_search, container, false);
        Button button = (Button) inflatedView.findViewById(R.id.search_button);
        search=(EditText)inflatedView.findViewById(R.id.search);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                SharedPreferences.Editor preferencesEditor = settings.edit();
                preferencesEditor.putString("scheduledetails_search", search.getText().toString().trim());
                preferencesEditor.commit();
                listener.OnItemSelected(" ");

            }
        });


        // Inflate the layout for this fragment
        return inflatedView;
    }


}
