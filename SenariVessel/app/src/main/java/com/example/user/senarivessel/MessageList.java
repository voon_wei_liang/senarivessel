package com.example.user.senarivessel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MessageList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessageList extends Fragment {
    ProgressDialog mDialog;
    //public String address="http://192.168.1.5:80/smartchat_cake/";
    MainActivity main=new MainActivity();
    public String address=main.address;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

    onListSelectedListener callBack;

    public interface onListSelectedListener{
        public void noInternet(String not);
    }

    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            callBack=(onListSelectedListener)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement onListSelectedListener");
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MessageList.
     */
    // TODO: Rename and change types and number of parameters
    public static MessageList newInstance(String param1, String param2) {
        MessageList fragment = new MessageList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MessageList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            new GetData().execute("haha");
        } else {
            // display error
            callBack.noInternet(" ");
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message_list, container, false);
    }

    private class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... data) {

            return RunPost();
        }


        private String RunPost() {
            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
            String id = settings.getString("id", null);
            String URL = address+"messages/getmessages_rest/"+id+".json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(URL);

            try {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);

            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }

            return xmlString;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result) {
            JSONObject jObject;
            JSONArray messageList;

            ListView list = (ListView)  getActivity().findViewById(R.id.Message);

            HashMap<String, String> map;

            String show = "";
            try {

                jObject = new JSONObject(result);

                messageList = jObject.getJSONArray("messages");
                for (int i = 0; i < messageList.length(); i++) {
                    JSONObject messages = messageList.getJSONObject(i);
                    JSONObject Message = messages.getJSONObject("Message");
                    String message_content = Message.getString("message");
                    String date_time = Message.getString("date_time");

                    JSONObject Vessel = messages.getJSONObject("Vessel");
                    String vessel_name = Vessel.getString("name");

                    map = new HashMap<String, String>();
                    map.put("Message", message_content);
                    map.put("VesselName", vessel_name);
                    map.put("DateTime",date_time);

                    mylist.add(map);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SimpleAdapter mSchedule = new SimpleAdapter(getActivity(), mylist, R.layout.message_list,
                    new String[]{"Message","VesselName", "DateTime"}, new int[]{R.id.message,R.id.vessel_name, R.id.date_time});
            list.setAdapter(mSchedule);
            mDialog.dismiss();
        }
    }

}
