package com.example.user.senarivessel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SendMessage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SendMessage extends Fragment {
    ProgressDialog mDialog;
    MainActivity main=new MainActivity();
    public String address=main.address;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String option="one"; //one is get list,two is post data
    EditText message;
    int selected;
    String[] parts1;
    String[] parts21;
    String messages;
    String vessel_name;

    onListSelectedListener callBack;

    public interface onListSelectedListener{
        public void OnItemSelected(String link);
        public void noInternet(String not);
    }

    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            callBack=(onListSelectedListener)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement onListSelectedListener");
        }
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SendMessage.
     */
    // TODO: Rename and change types and number of parameters
    public static SendMessage newInstance(String param1, String param2) {
        SendMessage fragment = new SendMessage();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SendMessage() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        List<String> id_items=new ArrayList<String>();
        List<String> name_items=new ArrayList<String>();
        // Inflate the layout for this fragment
        View inflatedView=inflater.inflate(R.layout.fragment_send_message, container, false);

        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            //Populate the spinner in the fragment
            new GetData().execute("run","","");
        } else {
            // display error
            callBack.noInternet(" ");
        }

        // Inflate the layout for this fragment
        return inflatedView;
    }

    private class GetData extends AsyncTask<String, String, String>  {

        @Override
        protected String doInBackground(String... data) {

            String returnData = "";

            return GetList("","","");//get the vessel list and put to spinner

        }

        private String GetList(String option,String messageValue,String vesselValue) {
            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
            String id = settings.getString("id", null);
            String URL = address+"users/getlistmessenger_rest/"+id+".json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(URL);

            try {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);


            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }


            return xmlString;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result) {

            String id="";
            String name="";

            JSONObject jObject;
            JSONArray vesselList;


            String show = "";
            String removeBracket;
            String removeDoubleQuote;
            String[] parts;
            String removeBracket2;
            String removeDoubleQuote2;
            String[] parts2;
            try {
//                Log.d("testing3", xmlString);
                jObject = new JSONObject(result);

                removeBracket=jObject.getString("id").substring(1,jObject.getString("id").length()-1);
                removeDoubleQuote=removeBracket.replace("\"","");
                parts=removeDoubleQuote.split(",");

                removeBracket2=jObject.getString("vesselname").substring(1,jObject.getString("vesselname").length()-1);
                removeDoubleQuote2=removeBracket2.replace("\"","");
                parts2=removeDoubleQuote2.split(",");

                for(int x=0;x< Array.getLength(parts);x++) {
                    id+=parts[x]+",";
                    name+=parts2[x]+",";
                }


                SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                SharedPreferences.Editor preferencesEditor = settings.edit();
                preferencesEditor.putString("vessel_id_messenger", id);
                preferencesEditor.commit();

                preferencesEditor.putString("vessel_name_messenger", name);
                preferencesEditor.commit();




            } catch (JSONException e) {
                e.printStackTrace();
            }
            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
            String ids = "";
            String names ="";
            ids = settings.getString("vessel_id_messenger", null);
            names = settings.getString("vessel_name_messenger", null);

            parts1=name.split(",");
            parts21=name.split(",");
            //Log.d("testing7", parts2[0]);

            Spinner vessel = (Spinner) getActivity().findViewById(R.id.vessels);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                    android.R.layout.simple_spinner_item, parts21);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            vessel.setAdapter(dataAdapter);

            vessel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    selected = position;
                    //Log.d("testing7", selected);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            Button button = (Button) getActivity().findViewById(R.id.send_button);
            message=(EditText)getActivity().findViewById(R.id.message);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    messages = message.getText().toString().trim();
                    vessel_name=parts21[selected];
                    new PostData().execute(messages,vessel_name);
                }
            });
            mDialog.dismiss();
        }


//        public void onItemSelected(AdapterView parent, View view, int position, long id) {
//            // On selecting a spinner item
//            String item = parent.getItemAtPosition(position).toString();
//
//            Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
//        }

//        @Override
//        public void onNothingSelected(AdapterView<?> parent) {
//
//        }
    }

    private class PostData extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... data) {

            String returnData = "";


            return RunPost(data[0],data[1]);
        }


        private String RunPost(String message,String vessel_name) {
            String URL = address+"messages/sendtovessel_rest.json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost post = new HttpPost(URL);

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                String user_id = settings.getString("id", null);

                nameValuePairs.add(new BasicNameValuePair("data[Message][message]", message));
                nameValuePairs.add(new BasicNameValuePair("data[Message][Vessel]", vessel_name));
                nameValuePairs.add(new BasicNameValuePair("data[Message][sender]", user_id));

                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(post);

                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);


            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }
            return xmlString;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result)
        {
            mDialog.dismiss();
            Toast.makeText(getActivity(), "Sending messages...",
                    Toast.LENGTH_LONG).show();
            Toast.makeText(getActivity(), "Message sent.",
                    Toast.LENGTH_LONG).show();
            callBack.OnItemSelected(" ");
        }
    }
}


