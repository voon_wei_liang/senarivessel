package com.example.user.senarivessel;

import android.app.Activity;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ScheduleDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScheduleDetails extends Fragment {
    ProgressDialog mDialog;
    MainActivity main=new MainActivity();
    public String address=main.address;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    TextView text;
    String message="";

    onListSelectedListener callBack;

    public interface onListSelectedListener{
        public void OnItemSelected(String link);
        public void noInternet(String not);
    }

    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            callBack=(onListSelectedListener)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement onListSelectedListener");
        }
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScheduleDetails.
     */
    // TODO: Rename and change types and number of parameters
    public static ScheduleDetails newInstance(String param1, String param2) {
        ScheduleDetails fragment = new ScheduleDetails();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ScheduleDetails() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            new GetData2().execute("run"," "," ");
        } else {
            // display error
            callBack.noInternet(" ");
        }
        View inflatedView=inflater.inflate(R.layout.fragment_schedule_details, container, false);
        //messages for this
        //new GetData().execute("run"," "," ");

        // Inflate the layout for this fragment
        return inflatedView;
    }



    private class GetData2 extends AsyncTask<String, String, String>  {

        SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
        String vesselName = settings.getString("scheduledetails_vesselname", null);
        String agentName = settings.getString("scheduledetails_shippingagentname", null);
        String id = settings.getString("id", null);
        @Override
        protected String doInBackground(String... data) {

            String returnData = "";
            return GetList("","","");//get the vessel list and put to spinner

        }

        private String GetList(String option,String messageValue,String vesselValue) {
            String URL = address+"users/getsubscribedlist_rest/"+id+".json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost post = new HttpPost(URL);

            try {
                HttpResponse response = httpclient.execute(post);
                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);

            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }


            return xmlString;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result) {
            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
            SharedPreferences.Editor preferencesEditor = settings.edit();
            String subscribed="false";
            JSONObject jObject;
            JSONArray vesselList;
            Button button = (Button) getActivity().findViewById(R.id.subscribe_button);
            settings = getActivity().getSharedPreferences("vesselappdata", 0);
            String is_subscribed = settings.getString("is_subscribed", null);
            TextView vesselName_text=(TextView)getActivity().findViewById(R.id.vessel_name);
            TextView shippingAgentName_text=(TextView)getActivity().findViewById(R.id.shipping_agent_name);
            TextView status_text=(TextView)getActivity().findViewById(R.id.status);
            TextView SCN_text=(TextView)getActivity().findViewById(R.id.SCN);
            TextView ETA_text=(TextView)getActivity().findViewById(R.id.ETA);
            TextView ETB_text=(TextView)getActivity().findViewById(R.id.ETB);

            HashMap<String, String> map;

            String show = "";
            try {

                jObject = new JSONObject(result);

                vesselList = jObject.getJSONArray("vessel");
                for (int i = 0; i < vesselList.length(); i++) {
                    JSONObject vessels = vesselList.getJSONObject(i);
                    if(vesselName.equals(vessels.getString("name"))){
                        subscribed="true";
                    }
                }
                preferencesEditor.putString("is_subscribed", subscribed);
                preferencesEditor.commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            vesselName_text.setText(vesselName);
            shippingAgentName_text.setText(agentName);
            status_text.setText("Status: "+settings.getString("scheduledetails_status", null));
            SCN_text.setText("SCN: "+settings.getString("scheduledetails_SCN", null));
            ETA_text.setText(settings.getString("scheduledetails_ETA", null));
            ETB_text.setText(settings.getString("scheduledetails_ETB", null));

            if(settings.contains("is_subscribed")) {
                if (subscribed.equals("false")) {
                    button.setText("Favourite");
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new PostData().execute("", "");
                        }
                    });
                } else {
                    button.setText("Un-Favourite");
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            new PostData2().execute("", "");
                        }
                    });
                }
            }else{
                button.setText("Favourite");
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new PostData().execute("", "");
                    }
                });
            }
            mDialog.dismiss();
        }


//        public void onItemSelected(AdapterView parent, View view, int position, long id) {
//            // On selecting a spinner item
//            String item = parent.getItemAtPosition(position).toString();
//
//            Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
//        }

//        @Override
//        public void onNothingSelected(AdapterView<?> parent) {
//
//        }
    }

//    //get the messages
//    private class GetData extends AsyncTask<String, String, String> {
//        SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
//        String vesselName = settings.getString("scheduledetails_vesselname", null);
//        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
//        @Override
//        protected String doInBackground(String... data) {
//
//            Log.d("sometas","sometas");
//            String returnData;
//
//            return RunPost();
//        }
//
//
//        private String RunPost() {
//            String URL = address+"messages/getmessages_rest/50.json";
//            String xmlString = "";
//
//            HttpParams httpParameters = new BasicHttpParams();
//            int timeoutConnection = 10000; // 10 second timeout for connecting
//            // to site
//            HttpConnectionParams.setConnectionTimeout(httpParameters,
//                    timeoutConnection);
//            int timeoutSocket = 30000; // 30 second timeout for getting a
//            // result, once connected
//            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//            HttpClient httpclient = new DefaultHttpClient(httpParameters);
//            HttpPost post = new HttpPost(URL);
//
//            try {
//                HttpResponse response = httpclient.execute(post);
//                HttpEntity r_entity = response.getEntity();
//                xmlString = EntityUtils.toString(r_entity);
//
//            } catch (ClientProtocolException e) {
//                xmlString = e.getMessage();
//            } catch (IOException e) {
//                xmlString = e.getMessage();
//            }
//
//
//            return xmlString;
//        }
//
//        protected void onPostExecute(String result) {
//                JSONObject jObject;
//                JSONArray vesselList;
//                Log.d("tests", result);
//
//                //ListView list = (ListView)  getActivity().findViewById(R.id.Messages);
//
//
//                HashMap<String, String> map;
//
//                String show = "";
//                try {
//
//                    jObject = new JSONObject(result);
//
//                    vesselList = jObject.getJSONArray("messages");
//                    for (int i = 0; i < vesselList.length(); i++) {
//                        JSONObject vessels = vesselList.getJSONObject(i);
//                        JSONObject Vessel = vessels.getJSONObject("Vessel");
//
//                        if(Vessel.getString("name").equals(vesselName)) {
//                            JSONObject Message = vessels.getJSONObject("Message");
//                            JSONObject User = vessels.getJSONObject("User");
//                            map = new HashMap<String, String>();
//                            map.put("Message", Message.getString("message"));
//                            map.put("Date & Time", Message.getString("date_time"));
//                            map.put("Send By", User.getString("username"));
//                            mylist.add(map);
//                        }
//
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                SimpleAdapter mSchedule = new SimpleAdapter(getActivity(), mylist, R.layout.row,
//                        new String[]{"Message", "Date & Time", "Send By"}, new int[]{R.id.name, R.id.ETA, R.id.ETB,});
//                //list.setAdapter(mSchedule);
//        }
//    }


    //subscribe
    private class PostData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... data) {

            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);

            String returnData = "";
            returnData = RunPost(data[0],data[1]);//run function RunPost

            JSONObject jObject;


            return returnData;
        }


        private String RunPost(String userName,String password) {
            String URL = address+"vessels/subscribe_user_rest.json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost post = new HttpPost(URL);

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                String vesselName = settings.getString("scheduledetails_vesselname", null);
                String userID= settings.getString("id", null);
                nameValuePairs.add(new BasicNameValuePair("data[Vessel][name]", vesselName));
                nameValuePairs.add(new BasicNameValuePair("data[Vessel][shipping_agent_name]", settings.getString("scheduledetails_shippingagentname", null).replace("Agent: ", "")));
                nameValuePairs.add(new BasicNameValuePair("data[Vessel][User][]", userID));

                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(post);

                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);


            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }
            return xmlString;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result) {
            mDialog.dismiss();
            Toast.makeText(getActivity(), "Adding to favourites...",
                    Toast.LENGTH_LONG).show();
            Toast.makeText(getActivity(), "Added vessel to favourites",
                    Toast.LENGTH_LONG).show();
            callBack.OnItemSelected(" ");

        }



    }

    private class PostData2 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... data) {

            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);

            String returnData = "";
            returnData = RunPost(data[0],data[1]);//run function RunPost

            JSONObject jObject;


            return returnData;
        }


        private String RunPost(String userName,String password) {
            String URL = address+"vessels/unsubscribe_user_rest.json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost post = new HttpPost(URL);

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                String vesselName = settings.getString("scheduledetails_vesselname", null);
                String userID= settings.getString("id", null);
                nameValuePairs.add(new BasicNameValuePair("data[Vessel][name]", vesselName));
                nameValuePairs.add(new BasicNameValuePair("data[Vessel][User][]", userID));

                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(post);

                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);


            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }
            return xmlString;
        }

        protected void onPostExecute(String result) {
            mDialog.dismiss();
            Toast.makeText(getActivity(), "Removing from favourites...",
                    Toast.LENGTH_LONG).show();
            Toast.makeText(getActivity(), "Removed vessel from favourites",
                    Toast.LENGTH_LONG).show();
            callBack.OnItemSelected(" ");
        }


    }


}
