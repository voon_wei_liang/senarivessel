package com.example.user.senarivessel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link SubscriptionList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubscriptionList extends Fragment {
    ProgressDialog mDialog;
    MainActivity main=new MainActivity();
    public String address=main.address;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<HashMap<String, String>> mylist;

    onListSelectedListener callBack2;

    public interface onListSelectedListener{
        public void onListSelected2(int pos);
        public void noInternet(String not);
    }

    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            callBack2=(onListSelectedListener)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement onListSelectedListener");
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubscriptionList.
     */


    // TODO: Rename and change types and number of parameters
    public static SubscriptionList newInstance(String param1, String param2) {
        SubscriptionList fragment = new SubscriptionList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SubscriptionList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putString("scheduledetails_vesselname", "");
        preferencesEditor.commit();
        preferencesEditor.putString("scheduledetails_shippingagentname", "");
        preferencesEditor.commit();

        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            new GetData().execute("haha");
        } else {
            // display error
            callBack2.noInternet(" ");
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_subscription_list, container, false);
    }

    private class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... data) {

            String returnData = "";


            returnData = RunPost();//testing purpose only
            return returnData;
        }


        private String RunPost() {
            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
            String id = "";
            String name ="";
            id = settings.getString("id", null);
            String URL = address+"users/getsubscribedlist_rest/"+id+".json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(URL);

            try {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);

            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }

            return xmlString;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result) {
            JSONObject jObject;
            JSONArray vesselList;
            mylist = new ArrayList<HashMap<String, String>>();

            ListView list = (ListView)  getActivity().findViewById(R.id.subscription_list);


            HashMap<String, String> map;


            String show = "";
            try {
                jObject = new JSONObject(result);

                vesselList = jObject.getJSONArray("vessel");
                for (int i = 0; i < vesselList.length(); i++) {
                    JSONObject vessels = vesselList.getJSONObject(i);
                    String vessel_name = vessels.getString("name");
                    String agent_name = vessels.getString("shipping_agent_name");

                    map = new HashMap<String, String>();
                    map.put("name", vessel_name);
                    map.put("shipping_agent", agent_name);
                    mylist.add(map);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            SimpleAdapter mSchedule = new SimpleAdapter(getActivity(), mylist, R.layout.subscription_list,
                    new String[]{"name","shipping_agent"}, new int[]{R.id.name, R.id.shipping_agent});
            list.setAdapter(mSchedule);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> arg0, View view, int pos,
                                        long arg3) {
                    SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                    SharedPreferences.Editor preferencesEditor = settings.edit();
                    preferencesEditor.putString("scheduledetails_vesselname", mylist.get(pos).get("name"));
                    preferencesEditor.commit();
                    preferencesEditor.putString("scheduledetails_shippingagentname", mylist.get(pos).get("shipping_agent"));
                    preferencesEditor.commit();
                    callBack2.onListSelected2(pos);
                }
            });
            mDialog.dismiss();
        }
    }

}
