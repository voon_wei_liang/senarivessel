package com.example.user.senarivessel;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;


public class MainActivity extends ActionBarActivity implements Subscription.onListSelectedListener,SendMessage.onListSelectedListener,ScheduleDetails.onListSelectedListener,MessageList.onListSelectedListener,ScheduleList.onListSelectedListener,SubscriptionList.onListSelectedListener,Login.OnItemSelectedListener,Signup.OnItemSelectedListener,Search.OnItemSelectedListener {
    //    public String address="http://192.168.1.5:80/smartchat_cake/";
    public String address="http://smartchats.esy.es/";
    String value="";
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "1";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    String SENDER_ID = "796867846503";

    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    Context context;

    String regid;

    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String mActivityTitle;
    public String Some;
    ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.mycolor)));
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                registerInBackground();
            }
        } else {
            //Log.i(TAG, "No valid Google Play Services APK found.");
        }

        mDrawerList = (ListView)findViewById(R.id.navList);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        addDrawerItems();
        setupDrawer();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        String menuFragment = getIntent().getStringExtra("menuFragment");

        Fragment fragment;
        FragmentManager fragmentManager;

        if (menuFragment != null)
        {
            if (menuFragment.equals("notification"))
            {
                fragment = new MessageList();
                getSupportActionBar().setTitle("Message List");
                fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            }
        }
        else
        {
            fragment = new ScheduleList();
            getSupportActionBar().setTitle("Schedule List");
            fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
        }


    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

//    public void viewScheduleDetails(){
//        Fragment fragment=new ScheduleDetails();
//        FragmentManager fragmentManager;
//
//        fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
//    }

    private void addDrawerItems() {
        SharedPreferences settings = getSharedPreferences("vesselappdata", 0);
        String is_login = settings.getString("is_login", null);
        String o = settings.getString("id", null);
        String t = settings.getString("is_login", null);
        String w = settings.getString("is_admin", null);
        mylist = new ArrayList<HashMap<String, String>>();
        HashMap<String, String> map;
        String loginImage="";


        if(settings.contains("is_login")) {

            if (is_login.equals("true")) {
                value="Logout";//user logged in so display logout
                loginImage=Integer.toString(R.drawable.logout);
            } else {
                value="Login";
                loginImage=Integer.toString(R.drawable.login);
            }
        }else{
            value="login";
            SharedPreferences.Editor preferencesEditor = settings.edit();
            preferencesEditor.putString("is_login", "false");
            preferencesEditor.commit();
        }

        final ArrayList<String> sideDrawerArray = new ArrayList<String>();
        map = new HashMap<String, String>();
        map.put("name","Schedule List" );
        map.put("image", Integer.toString(R.drawable.schedule_list));
        mylist.add(map);

        map = new HashMap<String, String>();
        map.put("name","Search" );
        map.put("image", Integer.toString(R.drawable.search));
        mylist.add(map);

        if(settings.contains("id")) {
            if (w.equals("1")) {
                map = new HashMap<String, String>();
                map.put("name","Send Message");
                map.put("image", Integer.toString(R.drawable.send_message));
                mylist.add(map);
            }
        }
        if(settings.contains("id")) {
            if (!t.equals("true")) {
                map = new HashMap<String, String>();
                map.put("name","Signup");
                map.put("image", Integer.toString(R.drawable.signup));
                mylist.add(map);
            }else{
                map = new HashMap<String, String>();
                map.put("name","Favourites");
                map.put("image", Integer.toString(R.drawable.subscription_list));
                mylist.add(map);
                map = new HashMap<String, String>();
                map.put("name","Message List");
                map.put("image", Integer.toString(R.drawable.message));
                mylist.add(map);
            }
        }else{
            map = new HashMap<String, String>();
            map.put("name","Signup");
            map.put("image", Integer.toString(R.drawable.signup));
            mylist.add(map);
        }
        map = new HashMap<String, String>();
        map.put("name",value);
        map.put("image", loginImage);
        mylist.add(map);

        map = new HashMap<String, String>();
        map.put("name","About" );
        map.put("image", Integer.toString(R.drawable.about));
        mylist.add(map);

        SimpleAdapter mSchedule = new SimpleAdapter(this, mylist, R.layout.drawer,
                new String[]{"image","name"}, new int[]{R.id.icon,R.id.name});

        mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, sideDrawerArray);
        mDrawerList.setAdapter(mSchedule);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Fragment fragment;
                FragmentManager fragmentManager;

                SharedPreferences settings = getSharedPreferences("vesselappdata", 0);
                SharedPreferences.Editor preferencesEditor = settings.edit();

                switch (mylist.get(position).get("name")) {
                    case "Schedule List":
                        fragment = new ScheduleList();

                        getSupportActionBar().setTitle("Schedule List");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "Login":

                        fragment = new Login();

                        if (value.equals("login")) {
                            fragment = new Login();
                        } else {

                        }
                        getSupportActionBar().setTitle("Login");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "Search":

                        fragment = new Search();
                        fragmentManager = getFragmentManager();
                        getSupportActionBar().setTitle("Search");
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "Logout":
                        new GetData().execute("haha");

                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "Favourites":
                        if (settings.getString("is_login", null).equals("true")) {
                            fragment = new SubscriptionList();
                        } else {
                            fragment = new Signup();
                        }
                        getSupportActionBar().setTitle("Favourites");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "Send Message":
                        fragment = new SendMessage();
                        getSupportActionBar().setTitle("Send Message");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "Message List":
                        fragment = new MessageList();
                        getSupportActionBar().setTitle("Message List");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "Signup":
                        fragment = new Signup();
                        getSupportActionBar().setTitle("Signup");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    case "About":
                        fragment = new About();
                        getSupportActionBar().setTitle("About");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;

                    default:
                        fragment = new ScheduleList();
                        getSupportActionBar().setTitle("Schedule List");
                        fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
                        mDrawerLayout.closeDrawer(mDrawerList);
                        break;
                }
            }
        });
    }



    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                addDrawerItems();


                getSupportActionBar().setTitle("Senari Vessel");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void noInternet(String link) {
        Fragment fragment;
        FragmentManager fragmentManager;
        fragment = new NoInternet();
        getSupportActionBar().setTitle("No Internet");
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void OnItemSelected(String link) {
        Fragment fragment;
        FragmentManager fragmentManager;
        fragment = new ScheduleList();
        getSupportActionBar().setTitle("Schedule List");
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void OnItemSelectedLogin(String link) {
        Fragment fragment;
        FragmentManager fragmentManager;
        fragment = new Login();
        getSupportActionBar().setTitle("Login");
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void onListSelected(int pos) {

        //set index in shared preference so that the detail page display the right content
        SharedPreferences settings = getSharedPreferences("vesselappdata", 0);

        Bundle bundle=new Bundle();
        bundle.putString("message","yelow world");

        Fragment fragment;
        FragmentManager fragmentManager;

        String is_login = settings.getString("is_login", null);

        if(is_login.equals("true")) {
            getSupportActionBar().setTitle("Schedule Details");
            fragment = new ScheduleDetails();
        }else{
            getSupportActionBar().setTitle("Login");
            fragment = new Login();
        }

        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

    }

    public void signup(String pos) {

        Fragment fragment;
        FragmentManager fragmentManager;
        fragment = new Signup();
        getSupportActionBar().setTitle("Signup");
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

    }

    @Override
    public void onListSelected2(int pos) {
        //set index in shared preference so that the detail page display the right content
        SharedPreferences settings = getSharedPreferences("vesselappdata", 0);

        Bundle bundle=new Bundle();
        bundle.putString("message","yelow world");

        Fragment fragment;
        FragmentManager fragmentManager;

        String is_login = settings.getString("is_login", null);

        if(is_login.equals("true")) {
            getSupportActionBar().setTitle("Subscription");
            fragment = new Subscription();
        }else{
            getSupportActionBar().setTitle("Login");
            fragment = new Login();
        }

        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check device for Play Services APK.
        checkPlayServices();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences settings = getSharedPreferences("vesselappdata", 0);
        //int appVersion = getAppVersion(context);

        //Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString(PROPERTY_REG_ID, regId);
        //editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences settings = getSharedPreferences("vesselappdata", 0);
        String registrationId = settings.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            //Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = settings.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        //int currentVersion = getAppVersion(context);
//        if (registeredVersion != currentVersion) {
//            //Log.i(TAG, "App version changed.");
//            return "";
//        }
        return registrationId;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                //mDisplay.append(msg + "\n");
            }
        }.execute(null, null, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }


    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
        // Your implementation here.
    }





    private class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... data) {

            SharedPreferences settings = getSharedPreferences("vesselappdata", 0);

            String returnData = "";
            returnData = RunPost();//run function RunPost

            SharedPreferences.Editor preferencesEditor = settings.edit();
            preferencesEditor.putString("is_login", "false");
            preferencesEditor.commit();

            return returnData;
        }


        private String RunPost() {
            SharedPreferences settings = getSharedPreferences("vesselappdata", 0);
            String URL = address+"users/logout_rest/"+settings.getString("id", null)+".json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(URL);

            try {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);

            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }
            return xmlString;
        }

        protected void onPostExecute(String result) {
            SharedPreferences settings = getSharedPreferences("vesselappdata", 0);
            SharedPreferences.Editor preferencesEditor = settings.edit();
            Toast.makeText(getApplicationContext(), "Logged out.",
                    Toast.LENGTH_LONG).show();
            Fragment fragment;
            FragmentManager fragmentManager;
            fragment = new ScheduleList();
            fragmentManager = getFragmentManager();
            getSupportActionBar().setTitle("Schedule List");
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
            preferencesEditor.putString("id", "");
            preferencesEditor.commit();
            preferencesEditor.putString("is_login", "");
            preferencesEditor.commit();
            preferencesEditor.putString("is_admin", "");
            preferencesEditor.commit();
        }


    }
}


