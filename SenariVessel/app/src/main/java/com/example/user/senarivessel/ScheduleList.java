package com.example.user.senarivessel;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class ScheduleList extends Fragment {
    ProgressDialog mDialog;
    //public String address="http://192.168.1.5:80/smartchat_cake/";
    MainActivity main=new MainActivity();
    public String address=main.address;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ScheduleList.
     */

    onListSelectedListener callBack;

    public interface onListSelectedListener{
        public void onListSelected(int pos);
        public void noInternet(String not);
    }

    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            callBack=(onListSelectedListener)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+"must implement onListSelectedListener");
        }
    }

    // TODO: Rename and change types and number of parameters
    public static ScheduleList newInstance(String param1, String param2) {
        ScheduleList fragment = new ScheduleList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ScheduleList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflatedView=inflater.inflate(R.layout.fragment_schedule_details, container, false);
        ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            new GetData().execute("haha");
        } else {
            // display error
            callBack.noInternet(" ");
        }

        SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putString("scheduledetails_vesselname", "");
        preferencesEditor.commit();
        preferencesEditor.putString("scheduledetails_shippingagentname", "");
        preferencesEditor.commit();



        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_schedule_list, container, false);
    }

    private class GetData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... data) {

            return RunPost();
        }


        private String RunPost() {
            String URL = address+"vesselvoyageschedules.json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpGet httpget = new HttpGet(URL);

            try {
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);

            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }

            return xmlString;
        }
        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result) {
            JSONObject jObject;
            JSONArray vesselList;

            if(result.length()<10){
                callBack.noInternet(" ");
            }else {

                String image = Integer.toString(R.drawable.scheduled);


                ListView list = (ListView) getActivity().findViewById(R.id.Schedule);

                HashMap<String, String> map;

                SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                SharedPreferences.Editor preferencesEditor = settings.edit();

                String show = "";
                try {

                    jObject = new JSONObject(result);

                    vesselList = jObject.getJSONArray("vesselvoyageschedules");
                    for (int i = 0; i < vesselList.length(); i++) {
                        JSONObject vessels = vesselList.getJSONObject(i);
                        JSONObject Vessel = vessels.getJSONObject("Vesselvoyageschedule");
                        String scn = Vessel.getString("scn");
                        show = show + scn + "\n";

                        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        Calendar c = Calendar.getInstance();
                        //System.out.println("Current time => " + c.getTime());

                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        String formattedDate = df.format(c.getTime());

                        try {
                            Date inputDate = new Date();
                            if (Vessel.getString("depart_date").equals("0000-00-00") || Vessel.getString("depart_date").equals("00:00")) {
                                inputDate = fmt.parse("1990-01-01 06:08");
                            } else {
                                inputDate = fmt.parse(Vessel.getString("depart_date") + " " + Vessel.getString("depart_date"));
                            }

                            Date inputDate2 = new Date();
                            if (Vessel.getString("berth_date").equals("0000-00-00") || Vessel.getString("berth_time").equals("00:00")) {
                                inputDate2 = fmt.parse("1990-01-01 06:08");
                            } else if (Vessel.getString("berth_time").equals("")) {
                                inputDate2 = fmt.parse(Vessel.getString("berth_date") + " 00:00");
                            } else {
                                inputDate2 = fmt.parse(Vessel.getString("berth_date") + " " + Vessel.getString("berth_time"));
                            }

                            Date current = fmt.parse(formattedDate);
                            //the one in small bracket is older than the outside
                            if (inputDate2.compareTo(current) <= 0) {
                                image = Integer.toString(R.drawable.berthed);
                            }

                            if (inputDate.compareTo(current) <= 0) {
                                image = Integer.toString(R.drawable.departed);
                            }

                            if (Vessel.getString("berth_date").equals("0000-00-00") || Vessel.getString("first_lift_date").equals("0000-00-00") || Vessel.getString("depart_date").equals("0000-00-00")) {
                                image = Integer.toString(R.drawable.scheduled);
                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        map = new HashMap<String, String>();
                        map.put("image", image);
                        map.put("name", Vessel.getString("ves_name"));
                        map.put("ETA", "ETA: " + Vessel.getString("eta_date") + "   " + Vessel.getString("eta_time"));
                        map.put("ETB", "ETB: " + Vessel.getString("etb_date") + "   " + Vessel.getString("etb_time"));
                        map.put("SCN", Vessel.getString("scn"));
                        map.put("shipping_agent", "Agent: " + Vessel.getString("agent_name"));

                        if (settings.contains("scheduledetails_search")) {
                            if (Vessel.getString("scn").toLowerCase().contains(settings.getString("scheduledetails_search", null)) || Vessel.getString("ves_name").toLowerCase().contains(settings.getString("scheduledetails_search", null)) || Vessel.getString("agent_name").toLowerCase().contains(settings.getString("scheduledetails_search", null))) {
                                mylist.add(map);
                            }
                        } else {
                            mylist.add(map);
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                preferencesEditor.putString("scheduledetails_search", "");
                preferencesEditor.commit();
                SimpleAdapter mSchedule = new SimpleAdapter(getActivity(), mylist, R.layout.row,
                        new String[]{"image", "name", "ETA", "ETB", "shipping_agent"}, new int[]{R.id.icon, R.id.name, R.id.ETA, R.id.ETB, R.id.shipping_agent});
                list.setAdapter(mSchedule);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View view, int pos,
                                            long arg3) {
                        //Toast.makeText(getActivity(), mylist.get(pos).get("name"), Toast.LENGTH_SHORT).show();
                        SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                        SharedPreferences.Editor preferencesEditor = settings.edit();
                        preferencesEditor.putString("scheduledetails_vesselname", mylist.get(pos).get("name"));
                        preferencesEditor.commit();
                        preferencesEditor.putString("scheduledetails_shippingagentname", mylist.get(pos).get("shipping_agent"));
                        preferencesEditor.commit();
                        if (mylist.get(pos).get("image").equals(Integer.toString(R.drawable.berthed))) {
                            preferencesEditor.putString("scheduledetails_status", "Berthed");
                            preferencesEditor.commit();
                        } else if (mylist.get(pos).get("image").equals(Integer.toString(R.drawable.departed))) {
                            preferencesEditor.putString("scheduledetails_status", "Departed");
                            preferencesEditor.commit();
                        } else {
                            preferencesEditor.putString("scheduledetails_status", "Scheduled");
                            preferencesEditor.commit();
                        }
                        preferencesEditor.putString("scheduledetails_SCN", mylist.get(pos).get("SCN"));
                        preferencesEditor.commit();
                        preferencesEditor.putString("scheduledetails_ETA", mylist.get(pos).get("ETA"));
                        preferencesEditor.commit();
                        preferencesEditor.putString("scheduledetails_ETB", mylist.get(pos).get("ETA"));
                        preferencesEditor.commit();
                        //                    Fragment fragment=new ScheduleDetails();
                        //
                        //
                        //                    FragmentTransaction t=getActivity().getFragmentManager().beginTransaction();
                        //                    fragment=new ScheduleDetails();
                        //                    t.replace(R.id.content_frame, fragment);
                        //                    t.addToBackStack(null);
                        //                    t.commit();
                        callBack.onListSelected(pos);
                        //Here pos is the position of row clicked

                    }
                });
            }
            mDialog.dismiss();
        }
    }

}
