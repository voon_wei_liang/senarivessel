package com.example.user.senarivessel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link Signup#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Signup extends Fragment {
    ProgressDialog mDialog;
    MainActivity main=new MainActivity();
    public String address=main.address;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String userNameValue;
    String passwordValue;
    String emailValue;

    EditText userName;
    EditText password;
    EditText email;



    // ...
    // Define the listener of the interface type
    // listener is the activity itself
    private OnItemSelectedListener listener;

    // Define the events that the fragment will use to communicate
    public interface OnItemSelectedListener {
        public void OnItemSelectedLogin(String link);
        public void noInternet(String not);
    }

    // Store the listener (activity) that will have events fired once the fragment is attached
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnItemSelectedListener) {
            listener = (OnItemSelectedListener) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Signup.
     */
    // TODO: Rename and change types and number of parameters
    public static Signup newInstance(String param1, String param2) {
        Signup fragment = new Signup();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public Signup() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflatedView=inflater.inflate(R.layout.fragment_signup, container, false);
        Button button = (Button) inflatedView.findViewById(R.id.signup_button);
        userName=(EditText)inflatedView.findViewById(R.id.user_name);
        email=(EditText)inflatedView.findViewById(R.id.email);
        password=(EditText)inflatedView.findViewById(R.id.password);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userNameValue = userName.getText().toString().trim();
                emailValue = email.getText().toString().trim();
                passwordValue = password.getText().toString().trim();
                ConnectivityManager connMgr = (ConnectivityManager) getActivity()
                        .getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.isConnected()) {
                    // fetch data
                    new PostData().execute(userNameValue,emailValue,passwordValue);
                } else {
                    // display error
                    listener.noInternet(" ");
                }
            }
        });

        // Inflate the layout for this fragment
        return inflatedView;
    }

    private class PostData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... data) {

            SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);

            String returnData = "";
            returnData = RunPost(data[0],data[1],data[2]);//run function RunPost

            return returnData;
        }


        private String RunPost(String userName,String email,String password) {
            String URL = address+"users/add_rest.json";
            String xmlString = "";

            HttpParams httpParameters = new BasicHttpParams();
            int timeoutConnection = 10000; // 10 second timeout for connecting
            // to site
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            int timeoutSocket = 30000; // 30 second timeout for getting a
            // result, once connected
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            HttpClient httpclient = new DefaultHttpClient(httpParameters);
            HttpPost post = new HttpPost(URL);

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                SharedPreferences settings = getActivity().getSharedPreferences("vesselappdata", 0);
                String registration_id = settings.getString("registration_id", null);

                nameValuePairs.add(new BasicNameValuePair("data[User][username]", userName));
                nameValuePairs.add(new BasicNameValuePair("data[User][password]", password));
                nameValuePairs.add(new BasicNameValuePair("data[User][email]", email));
                nameValuePairs.add(new BasicNameValuePair("data[User][is_admin]","0"));

                post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(post);

                HttpEntity r_entity = response.getEntity();
                xmlString = EntityUtils.toString(r_entity);


            } catch (ClientProtocolException e) {
                xmlString = e.getMessage();
            } catch (IOException e) {
                xmlString = e.getMessage();
            }
            return xmlString;
        }

        protected void onPreExecute(){
            super.onPreExecute();
            mDialog=new ProgressDialog(getActivity());
            mDialog.setMessage("Please Wait....");
            mDialog.show();
        }

        protected void onPostExecute(String result) {
            mDialog.dismiss();
            listener.OnItemSelectedLogin("some link");
        }
    }

}
